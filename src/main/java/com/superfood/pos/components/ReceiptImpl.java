package com.superfood.pos.components;

import com.google.common.collect.ImmutableList;
import com.superfood.pos.api.Receipt;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Optional;

public class ReceiptImpl implements Receipt {

    private ImmutableList<Entry> items = ImmutableList.<Entry>builder().build();

    private final static DecimalFormat df2 = new DecimalFormat("$#.##");

    private long _countEntries(String code, Entry.EntryType type) {
        return items.stream()
                .filter((entry) -> entry.getCode().equals(code))
                .filter((entry) -> entry.getType() == type)
                .mapToLong(Entry::getQty)
                .sum();
    }


    @Override
    public long countEntries(String code) {
        return _countEntries(code, Entry.EntryType.Debit);
    }

    @Override
    public long countCredits(String code) {
        return _countEntries(code, Entry.EntryType.Credit);
    }

    @Override
    public void addEntry(Entry entry) {
        items = ImmutableList.<Entry>builder().addAll(items).add(entry).build();
    }

    @Override
    public int getSize() {
        return items.size();
    }

    @Override
    public BigDecimal calculateTotal() {


        System.out.println("**************************************************");
        System.out.println("*****               RECEIPT                  *****");
        System.out.println("**************************************************");

        Optional<BigDecimal> to = items.stream()
                .peek(entry -> {
                    System.out.println(entry.toString());
                })
                .map(entry -> entry.getPrice().multiply(BigDecimal.valueOf(entry.getQty())))
                .reduce((bd, count) -> count.add(bd));

        BigDecimal total = to.orElse(BigDecimal.ZERO).setScale(2, RoundingMode.HALF_EVEN);
        System.out.println("**************************************************");
        System.out.println("\t\t\tTOTAL:\t\t\t" + df2.format(total));

        return total;
    }
}
