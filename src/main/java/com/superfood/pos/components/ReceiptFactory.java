package com.superfood.pos.components;

import com.superfood.pos.api.Receipt;
import org.springframework.stereotype.Component;

@Component
public class ReceiptFactory {

    public Receipt getReceiptInstance(){
        return new ReceiptImpl();
    }

}
