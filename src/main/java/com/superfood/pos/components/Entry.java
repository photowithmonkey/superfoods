package com.superfood.pos.components;

import com.google.common.base.Strings;
import com.superfood.pos.util.TerminalColors;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.DecimalFormat;

// Holds individual receipt entry
// All the fields of this class are declared final (immutable)
// This class uses Lombok to bypass typical Java boilerplate

@Getter
@RequiredArgsConstructor
public class Entry {

    final private Logger logger = LoggerFactory.getLogger(Entry.class);

    public enum EntryType {Debit, Credit}

    final private String code;
    final private String name;
    final private BigDecimal price;
    final private EntryType type;
    final private Long qty;

    private final static DecimalFormat df2 = new DecimalFormat("#.##");

    // calc tab spacing for different length strings
    private String calcTab(String name) {
        int tabs = 3 - name.length() / 8;
        return Strings.repeat("\t", tabs);
    }

    // compare Entries by field values, vs. reference
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Entry) {
            Entry entryB = ((Entry) obj);
            return code.equals(entryB.getCode())
                    && price.equals(entryB.getPrice())
                    && name.equals(entryB.getName())
                    && type.equals(entryB.getType());
        }

        return false;
    }

    // Format the Entry values for "register tape" like output
    @Override
    public String toString() {
        String s = type == EntryType.Debit ? "+" : price.compareTo(BigDecimal.ZERO) < 0 ? "" : "+";

        if (type == EntryType.Debit)
            return code + "\t\t" + name + calcTab(name) + "\t" + s + df2.format(price) + TerminalColors.ANSI_RESET;
        else
            return "\t\t\t" + ((price.compareTo(BigDecimal.ZERO) < 0) ? TerminalColors.ANSI_GREEN + "Credit\t\t\t" : TerminalColors.ANSI_RED + "Surcharge\t\t") + "\t" + s + df2.format(price) + TerminalColors.ANSI_RESET;
    }

}
