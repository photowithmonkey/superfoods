package com.superfood.pos.components;

import com.superfood.pos.api.Register;
import com.superfood.pos.api.Handler;
import com.superfood.pos.api.Receipt;
import net.minidev.json.JSONObject;

import java.math.BigDecimal;
import java.util.List;

// Concrete Implementation of the Register interface

public class RegisterImpl implements Register {

    private final List<Handler> handlerInterfacesList;
    private final JSONObject pricingScheme;
    private final ReceiptFactory receiptFactory;

    private Handler startHandler; // reference to the first handler in the responsibility chain

    private Receipt currentReceipt = null;


    // pricingScheme - effective pricing scheme
    // receipt - instance of Receipt

    public RegisterImpl(JSONObject pricingScheme, List<Handler> handlerList, ReceiptFactory receiptFactory) {
        this.pricingScheme = pricingScheme;
        this.handlerInterfacesList = handlerList;
        this.receiptFactory = receiptFactory;
        if (handlerList.size() > 0) {
            setupHandlers();
        }
    }


    // "scan" a product to purchase

    @Override
    public void scan(String code /* unique product code */) throws Exception {

        if (currentReceipt == null) {
            currentReceipt = receiptFactory.getReceiptInstance();
        }

        // find the pricing category corresponding to the code parameter
        // pass the pricing category down the responsibility chain, until its claimed
        Handler handler = startHandler.willHandle((JSONObject) pricingScheme.get(code));
        if (handler == null) {
            // no handler available for the code
            throw new Exception("No price information found for item: " + code);
        }

        // call the selected Handler's handle method to price the item
        handler.handle(code, pricingScheme, currentReceipt);
    }

    // call the Receipt (register tape) to total up the items in the current session
    // this is a cash out transaction, so the receipt instance is set back to NULL

    @Override
    public BigDecimal getTotal() {

        if (currentReceipt == null)
            return BigDecimal.ZERO;

        BigDecimal total = currentReceipt.calculateTotal();
        currentReceipt = null; // end register session

        return total;
    }

    // iterates through the available Handlers, linking them together, hence a chain
    private void setupHandlers() {
        Handler current = null;
        for (Handler item : handlerInterfacesList) {
            if (current == null)
                startHandler = item; // store the first handler in the chain
            else
                current.setSuccessor(item); // link handlers uni-directionally

            current = item;
        }
    }
}
