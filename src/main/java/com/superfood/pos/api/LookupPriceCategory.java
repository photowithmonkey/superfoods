package com.superfood.pos.api;

import net.minidev.json.JSONObject;

@FunctionalInterface
public interface LookupPriceCategory {
    JSONObject getPriceCategory(String code);
}
