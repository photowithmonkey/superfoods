package com.superfood.pos.api;

import net.minidev.json.JSONObject;

public interface Handler {

    Handler willHandle(JSONObject offer);

    Handler getSuccessor();

    void setSuccessor(Handler successor);

    void handle(String code, JSONObject priceScheme, Receipt receipt);
}
