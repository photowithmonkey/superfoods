package com.superfood.pos.api;

import com.superfood.pos.components.Entry;

import java.math.BigDecimal;
import java.util.Optional;

public interface Receipt {

    long countEntries(String code);

    long countCredits(String code);

    void addEntry(Entry entry);

    int getSize();

    BigDecimal calculateTotal();
}
