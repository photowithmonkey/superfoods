package com.superfood.pos.api;

import net.minidev.json.JSONObject;

public interface CheckoutFactory {
    Register getRegisterInstance(JSONObject pricingScheme);
}
