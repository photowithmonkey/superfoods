package com.superfood.pos.api;

import java.math.BigDecimal;

public interface Register {

    void scan(String code) throws Exception;

    BigDecimal getTotal();

}
