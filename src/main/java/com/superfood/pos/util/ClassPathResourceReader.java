package com.superfood.pos.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;


// this Class takes care of reading a resource from the classpath
// this takes care of the issue of not being able to access the resource via File
// once it is added to the JAR.

public class ClassPathResourceReader {

    Logger logger = LoggerFactory.getLogger(ClassPathResourceReader.class);

    private String path;

    public ClassPathResourceReader(String path) {
        this.path = path;
    }

    public InputStream getInputStream() throws IOException {
        ClassPathResource cpr = new ClassPathResource(path);
        return cpr.getInputStream();
    }

    public String readContentAsString() {
        String data = "";
        try {
            byte[] bdata = FileCopyUtils.copyToByteArray(getInputStream());
            data = new String(bdata, StandardCharsets.UTF_8);
        } catch (IOException e) {
            logger.warn("IOException", e);
        }

        return data;
    }
}
