package com.superfood.pos;

import com.superfood.pos.api.CheckoutFactory;
import com.superfood.pos.demo.DemoCheckout;
import com.superfood.pos.demo.TestPricingSchemaBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

// main Spring Boot method - runs the main checkout sequence

@SpringBootApplication
public class PosApplication {

    static Logger logger = LoggerFactory.getLogger(PosApplication.class);

    @Bean
    DemoCheckout getDemoCheckout(CheckoutFactory checkoutFactory, TestPricingSchemaBean testPricingSchemaBean) {
        return new DemoCheckout(checkoutFactory, testPricingSchemaBean);
    }

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(PosApplication.class, args);
        DemoCheckout bean = context.getBean(DemoCheckout.class);
        try {
            // run prescribed checkout session
            bean.runCheckout();
        } catch (Exception e) {
            logger.error("Error, could not run checkout ");
        }
    }


}
