package com.superfood.pos.consts;

public class CommonPriceCategoryConst {
    public final static String NAME = "name";
    public final static String PRICE = "price";
    public final static String TYPE = "type";
}
