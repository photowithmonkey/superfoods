package com.superfood.pos.demo;

import com.superfood.pos.util.ClassPathResourceReader;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

// Load the test JSON with the pricing schema specified in the Exercise

@Component
public class TestPricingSchemaBean {

    Logger logger = LoggerFactory.getLogger(TestPricingSchemaBean.class);

    public JSONObject getTestPricingSchema() {
        try {
            ClassPathResourceReader classPathResourceReader = new ClassPathResourceReader("testPricingScheme.json");
            JSONParser parser = new JSONParser(JSONParser.DEFAULT_PERMISSIVE_MODE);
            // pass the InputStream for the JSON file to the json-smart parser to get the JSONObject
            return (JSONObject) parser.parse(classPathResourceReader.getInputStream());
        } catch (Exception e) {
            logger.error("Could not load test pricing schema");
        }

        return null;
    }

}
