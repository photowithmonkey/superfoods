package com.superfood.pos.demo;

import com.superfood.pos.api.Register;
import com.superfood.pos.api.CheckoutFactory;

import java.math.BigDecimal;


// run the sequence of scans prescribed in the Exercise

public class DemoCheckout {

    private CheckoutFactory checkoutFactory;
    private TestPricingSchemaBean testPricingSchemaBean;

    public DemoCheckout(CheckoutFactory checkoutFactory, TestPricingSchemaBean testPricingSchemaBean) {
        this.checkoutFactory = checkoutFactory;
        this.testPricingSchemaBean = testPricingSchemaBean;
    }


    public BigDecimal runCheckout() throws Exception {
        Register register = checkoutFactory.getRegisterInstance(testPricingSchemaBean.getTestPricingSchema());
        register.scan("1983");
        register.scan("4900");
        register.scan("8873");
        register.scan("6732");
        register.scan("0923");
        register.scan("1983");
        register.scan("1983");
        register.scan("1983");
        return register.getTotal();
    }
}
