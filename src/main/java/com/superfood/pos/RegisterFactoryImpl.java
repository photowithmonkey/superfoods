package com.superfood.pos;

import com.superfood.pos.api.Register;
import com.superfood.pos.api.CheckoutFactory;
import com.superfood.pos.api.Handler;
import com.superfood.pos.components.ReceiptFactory;
import com.superfood.pos.components.RegisterImpl;
import net.minidev.json.JSONObject;
import org.springframework.stereotype.Component;

import java.util.List;

// Spring Bean Factory return instances of Register class

@Component
public class RegisterFactoryImpl implements CheckoutFactory {

    // holder for price category handlers, loaded automatically from scanned Componenets
    // that implement the Handler interface by Spring.
    private final List<Handler> handlerList;
    private final ReceiptFactory receiptFactory;

    public RegisterFactoryImpl(List<Handler> handlerList, ReceiptFactory receiptFactory) {
        this.handlerList = handlerList;
        this.receiptFactory = receiptFactory;
    }

    // returns an instance of Register, initialized with the pricing scheme JSON
    @Override
    public Register getRegisterInstance(JSONObject pricingScheme) {
        return new RegisterImpl(pricingScheme, handlerList, receiptFactory);
    }
}
