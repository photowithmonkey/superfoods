package com.superfood.pos.pricecategories;

import com.superfood.pos.api.Handler;
import com.superfood.pos.api.LookupPriceCategory;
import com.superfood.pos.api.Receipt;
import com.superfood.pos.components.Entry;
import net.minidev.json.JSONObject;

import java.math.BigDecimal;
import java.util.Optional;

import static com.superfood.pos.consts.CommonPriceCategoryConst.NAME;
import static com.superfood.pos.consts.CommonPriceCategoryConst.PRICE;

// This is the base class that all Price Categories have to implement,
// it also implements the Handler interface to support the Chain of Responsibility

public abstract class PricingCategoryAbstract implements Handler {

    private Handler successor;

    public Handler getSuccessor() {
        return this.successor;
    }

    public void setSuccessor(Handler successor) {
        this.successor = successor;
    }

    public void handle(String code, JSONObject pricingScheme, Receipt receipt) {

        // Step 1. record a scanned item at regular pricing
        JSONObject priceCategory = ((JSONObject) pricingScheme.get(code));
        BigDecimal price = new BigDecimal(priceCategory.getAsString(PRICE));
        // check if the product code is already on the receipt
        // if it does, increment qty on existing item, otherwise add a new item to the receipt
        receipt.addEntry(new Entry(code, priceCategory.getAsString(NAME), price, Entry.EntryType.Debit, 1L));

        // Step 2. check to see if the purchased items meet special pricing conditions, if it does we  add a credit (or extra charge)
        Optional<Entry> priceAdjustment = this.calcSpecialPrice(code, priceCategory, getPriceCategoryFunc(pricingScheme), receipt);
        priceAdjustment.ifPresent(receipt::addEntry);
    }


    // decide if the implementing class will handle the price category,
    public Handler willHandle(JSONObject priceCategory) {

        // if this Handler instance can handle this price Category, go ahead and return the Handler instance
        if (customWillHandle(priceCategory)) {
            return this;
        }

        // check if this is the end of the Chain, and no Handler has been found
        if (getSuccessor() == null)
            return null;

        // if the next handler is available in the chain, pass the price category to it
        return getSuccessor().willHandle(priceCategory);
    }

    //  the implementing class decides if it can handle pricing category
    protected abstract boolean customWillHandle(JSONObject offer);

    //  the implementing class applies specific rules for figuring out price
    protected abstract Optional<Entry> calcSpecialPrice(String code, JSONObject priceScheme,
                                                        LookupPriceCategory priceCategoryFunc,
                                                        Receipt receipt);

    // We want to keep the implementing price category class from needing to know the details of the overall
   // price schema. So we just pass it a getter with a bound pricing schema.
    protected LookupPriceCategory getPriceCategoryFunc(JSONObject pricingSchema) {
        return code -> ((JSONObject) pricingSchema.get(code));
    }
}
