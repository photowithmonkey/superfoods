package com.superfood.pos.pricecategories;

import com.superfood.pos.api.LookupPriceCategory;
import com.superfood.pos.api.Receipt;
import com.superfood.pos.components.Entry;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Optional;

import static com.superfood.pos.consts.CommonPriceCategoryConst.*;

// Handle Additional Taxes price category where a purchased item incurs a surcharge

@Component
public class AdditionalTaxes extends PricingCategoryAbstract {

    Logger logger = LoggerFactory.getLogger(AdditionalTaxes.class);

    private final static String ADDITIONAL_TAX = "AddonTax";
    private final static String SURCHARGE = "surcharge";

    @Override
    protected boolean customWillHandle(JSONObject priceCategory) {
        return priceCategory.getAsString(TYPE).equals(ADDITIONAL_TAX);
    }

    @Override
    protected Optional<Entry> calcSpecialPrice(String code, JSONObject priceCategory, LookupPriceCategory priceCategoryFunc,
                                               Receipt receipt) {

        long countEntries = receipt.countEntries(code); // number of entries for code
        if (countEntries > 0) {
            BigDecimal price = new BigDecimal(priceCategory.getAsString(PRICE)); // base price
            BigDecimal surcharge = new BigDecimal(priceCategory.getAsString(SURCHARGE)); // calculate surcharge

            long countCredits = receipt.countCredits(code); // number of credits already entered

            if (countEntries - countCredits > 0) {
                // create an entry for balance of credits
                Entry e = new Entry(code, priceCategory.getAsString(NAME), price.multiply(surcharge),
                        Entry.EntryType.Credit, countEntries - countCredits);
                return Optional.of(e);
            }
            return Optional.empty();
        }

        logger.error("Purchased item not found on receipt, it should be there before special price is calculated");
        return Optional.empty();
    }
}
