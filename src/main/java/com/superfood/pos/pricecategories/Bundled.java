package com.superfood.pos.pricecategories;

import com.superfood.pos.api.LookupPriceCategory;
import com.superfood.pos.api.Receipt;
import com.superfood.pos.components.Entry;
import net.minidev.json.JSONObject;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Optional;

import static com.superfood.pos.consts.CommonPriceCategoryConst.*;

// Handle Bundled Price Category - Buying specific items together is cheaper

@Component
public class Bundled extends PricingCategoryAbstract {

    private final static String BUNDLED = "Bundled";
    private final static String BUNDLED_ITEM = "bundledItem";
    private final static String BUNDLED_PRICE = "bundledPrice";


    @Override
    protected boolean customWillHandle(JSONObject priceCategory) {
        return priceCategory.getAsString(TYPE).equals(BUNDLED);
    }


    @Override
    protected Optional<Entry> calcSpecialPrice(String code, JSONObject priceCategory,
                                               LookupPriceCategory boundPriceCategoryLookupFunc,
                                               Receipt receipt) {

        long countEntryA = receipt.countEntries(code); // number of entries with same code
        BigDecimal priceA = new BigDecimal(priceCategory.getAsString(PRICE)); // base price
        String boundItemCode = priceCategory.getAsString(BUNDLED_ITEM); // code of bound price item
        JSONObject boundItem = boundPriceCategoryLookupFunc.getPriceCategory(boundItemCode); // lookup bound item
        BigDecimal boundItemPrice = new BigDecimal(boundItem.getAsString(PRICE)); // get bound item price

        long countEntryB = receipt.countEntries(boundItemCode); // number of existing entries for bound item

        if (countEntryA > 0 && countEntryB > 0) { // both items are on the receipt
            long credits = (countEntryA + countEntryA) / 2; // number of eligible credits
            if (credits > 0) {
                long existingCredits = receipt.countCredits(code); // number of credits already issued
                if (credits - existingCredits > 0) {
                    //price for both items purchased together
                    BigDecimal offerPrice = new BigDecimal(priceCategory.getAsString(BUNDLED_PRICE));
                    // credit the difference in price for both vs individually
                    BigDecimal creditAmt = priceA.add(boundItemPrice).subtract(offerPrice).negate();
                    Entry e = new Entry(code, priceCategory.getAsString(NAME), creditAmt, Entry.EntryType.Credit,
                            credits - existingCredits);
                    return Optional.of(e);
                }
            }
        }

        return Optional.empty();
    }
}
