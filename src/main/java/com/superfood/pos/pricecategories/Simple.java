package com.superfood.pos.pricecategories;

import com.superfood.pos.api.LookupPriceCategory;
import com.superfood.pos.api.Receipt;
import com.superfood.pos.components.Entry;
import net.minidev.json.JSONObject;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.superfood.pos.consts.CommonPriceCategoryConst.TYPE;

// Handle the Simple pricing category, which does not get much handling since it is only the base
// price - already dealt with in the Base (PricingCategoryAbstract) class

@Component
public class Simple extends PricingCategoryAbstract {

    private static final String SIMPLE = "Simple";

    @Override
    protected boolean customWillHandle(JSONObject priceCategory) {
        return !priceCategory.containsKey(TYPE) || priceCategory.getAsString(TYPE).equals(SIMPLE);
    }

    @Override
    protected Optional<Entry> calcSpecialPrice(String code, JSONObject priceScheme,
                                               LookupPriceCategory priceCategoryFunc,
                                               Receipt receipt) {
        return Optional.empty();
    }
}
