package com.superfood.pos.pricecategories;

import com.superfood.pos.api.LookupPriceCategory;
import com.superfood.pos.api.Receipt;
import com.superfood.pos.components.Entry;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Optional;

import static com.superfood.pos.consts.CommonPriceCategoryConst.*;

// Handle Buy X Get Y Free Pricing Category

@Component
public class BuyXGetYFree extends PricingCategoryAbstract {

    private final static String BUYXGETYFREE = "BuyXGetYFree";
    private final static String BUYX = "buyX";
    private final static String GETY = "getY";

    private Logger logger = LoggerFactory.getLogger(BuyXGetYFree.class);

    @Override
    protected boolean customWillHandle(JSONObject priceCategory) {
        return (priceCategory.getAsString(TYPE).equals(BUYXGETYFREE));
    }


    @Override
    protected Optional<Entry> calcSpecialPrice(String code, JSONObject priceCategory, LookupPriceCategory priceCategoryFunc, Receipt receipt) {

        int buyX = priceCategory.getAsNumber(BUYX).intValue();
        int getY = priceCategory.getAsNumber(GETY).intValue();
        BigDecimal price = new BigDecimal(priceCategory.getAsString(PRICE));

        long countEntry = receipt.countEntries(code); // get the number of entries with same code
        if (countEntry > 0) {
            long credits = countEntry / (buyX + getY); // calculate total number of eligible credits
            if (credits > 0) {
                long countCredits = receipt.countCredits(code); // number of existing credits
                if (credits - countCredits > 0) {
                    // create a credit for the remaining balance of credits
                    Entry e = new Entry(code, priceCategory.getAsString(NAME), price.negate(), Entry.EntryType.Credit,
                            credits - countCredits);
                    return Optional.of(e);
                }
            }
            return Optional.empty();
        }

        logger.error("Purchased item not found on receipt, it should be there before special price is calculated");
        return Optional.empty();
    }
}
