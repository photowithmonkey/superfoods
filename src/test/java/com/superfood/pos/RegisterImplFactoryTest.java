package com.superfood.pos;

import com.superfood.pos.api.Register;
import com.superfood.pos.api.CheckoutFactory;
import com.superfood.pos.api.Handler;
import com.superfood.pos.components.ReceiptFactory;
import com.superfood.pos.components.RegisterImpl;
import com.superfood.pos.demo.TestPricingSchemaBean;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

@SpringBootTest
class RegisterImplFactoryTest {

    @MockBean
    private CheckoutFactory checkoutFactory;

    @Autowired
    private List<Handler> handlerList;

    @Autowired
    private ReceiptFactory receiptFactory;

    @Autowired
    private TestPricingSchemaBean testPricingSchemaBean;

    @Test
    void testHandlerInterfaceList() {
        assertEquals(4, handlerList.size());
    }


    @Test
    void getCheckout() {
        JSONObject testPricingSchema = testPricingSchemaBean.getTestPricingSchema();
        RegisterImpl testRegisterImpl = new RegisterImpl(testPricingSchema,  handlerList, receiptFactory);
        given(checkoutFactory.getRegisterInstance(testPricingSchema)).willReturn(testRegisterImpl);
        Register result = checkoutFactory.getRegisterInstance(testPricingSchema);
        assertEquals(testRegisterImpl, result);
    }
}