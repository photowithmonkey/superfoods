package com.superfood.pos.components;

import com.superfood.pos.api.Receipt;
import com.superfood.pos.testdatabeans.TestReceiptBean;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ReceiptImplTest {

    @Autowired
    TestReceiptBean testReceiptBean;

    private Receipt testReceipt;

    @BeforeEach
    void createTestRecipt() {
        testReceipt = testReceiptBean.getTestReceipt();
    }

    @Test
    void addEntry() {
        Receipt receipt = new ReceiptImpl();
        receipt.addEntry(new Entry("4900", "Salsa", new BigDecimal("3.49"), Entry.EntryType.Debit, 1L));
        receipt.addEntry(new Entry("8873", "Milk", new BigDecimal("2.49"), Entry.EntryType.Debit, 1L));
        receipt.addEntry(new Entry("6732", "Chips", new BigDecimal("2.49"), Entry.EntryType.Debit, 1L));
        assertEquals(3, receipt.getSize());
    }

    @Test
    void calculateTotal() {
        BigDecimal total1 = testReceipt.calculateTotal();
        assertEquals(new BigDecimal("30.37"), total1);
    }

    @Test
    void countEntries() {
        long entries = testReceipt.countEntries("1983");
        assertEquals(4, entries);
    }

    @Test
    void countCredits() {
        testReceipt.addEntry(new Entry("1983", "Toothbrush", new BigDecimal("1.99"), Entry.EntryType.Credit, 2L));
        long credits = testReceipt.countCredits("1983");
        assertEquals(3, credits);
    }

    @Test
    void getSize() {
        int size = testReceipt.getSize();
        assertEquals(11, size);
    }
}