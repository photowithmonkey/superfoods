package com.superfood.pos.components;

import com.superfood.pos.api.Register;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.mockito.Mockito.verify;

@SpringBootTest
class RegisterImplTest {

    @MockBean
    Register registerMock;

    @Test
    void scan() throws Exception {
        registerMock.scan("6732");
        verify(registerMock).scan("6732");
    }

    @Test
    void getTotal() {
        registerMock.getTotal();
        verify(registerMock).getTotal();
    }
}