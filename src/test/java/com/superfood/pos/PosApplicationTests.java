package com.superfood.pos;

import com.superfood.pos.api.CheckoutFactory;
import com.superfood.pos.demo.DemoCheckout;
import com.superfood.pos.demo.TestPricingSchemaBean;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class PosApplicationTests {

    @Autowired
    CheckoutFactory checkoutFactory;

    @Autowired
    TestPricingSchemaBean testPricingSchemaBean;

    @Test
    void operationsTest() throws Exception {
        DemoCheckout demoCheckout = new DemoCheckout(checkoutFactory, testPricingSchemaBean);
        BigDecimal total = demoCheckout.runCheckout();
        assertEquals(new BigDecimal("30.37"), total);
    }

}
