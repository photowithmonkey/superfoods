package com.superfood.pos.pricecategories;

import com.superfood.pos.components.Entry;
import com.superfood.pos.demo.TestPricingSchemaBean;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class SimpleTest {

    @Autowired
    TestPricingSchemaBean testPricingSchemaBean;

    private JSONObject testPricingSchema;

    private JSONObject testPriceCategory;

    private JSONObject testAntiPriceCategory;


    @BeforeEach
    public void setup() {
        testPricingSchema = this.testPricingSchemaBean.getTestPricingSchema();
        testPriceCategory = ((JSONObject) testPricingSchema.get("8873"));
        testAntiPriceCategory = ((JSONObject) testPricingSchema.get("6732"));
    }

    @Test
    void customWillHandle() {
        Simple simple = new Simple();
        boolean result = simple.customWillHandle(testPriceCategory);
        assertTrue(result);

        boolean result2 = simple.customWillHandle(testAntiPriceCategory);
        assertFalse(result2);
    }

    @Test
    void calcSpecialPrice() {
        Simple simple = new Simple();
        JSONObject priceCategory = (JSONObject) testPricingSchema.get("8873");
        Optional<Entry> optionalEntry = simple.calcSpecialPrice("8873", priceCategory, null, null);

        assertFalse(optionalEntry.isPresent());
    }

}