package com.superfood.pos.pricecategories;

import com.superfood.pos.api.Receipt;
import com.superfood.pos.components.Entry;
import com.superfood.pos.demo.TestPricingSchemaBean;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@SpringBootTest
class BuyXGetYFreeTest {

    @Autowired
    TestPricingSchemaBean testPricingSchemaBean;

    private JSONObject testPricingSchema;

    private JSONObject testPriceCategory;

    private JSONObject testAntiPriceCategory;

    @MockBean
    private Receipt receiptMock;

    @BeforeEach
    public void setup() {
        testPricingSchema = this.testPricingSchemaBean.getTestPricingSchema();
        testPriceCategory = ((JSONObject) testPricingSchema.get("1983"));
        testAntiPriceCategory = ((JSONObject) testPricingSchema.get("0923"));
    }

    @Test
    void customWillHandle() {
        BuyXGetYFree buyXGetYFree = new BuyXGetYFree();
        boolean result = buyXGetYFree.customWillHandle(testPriceCategory);
        assertTrue(result);

        boolean result2 = buyXGetYFree.customWillHandle(testAntiPriceCategory);
        assertFalse(result2);
    }

    @Test
    void calcSpecialPrice() {

        given(receiptMock.countEntries("1983")).willReturn(1L);

        BuyXGetYFree buyXGetYFree = new BuyXGetYFree();
        JSONObject priceCategory = ((JSONObject) testPricingSchema.get("1983"));

        Optional<Entry> entry = buyXGetYFree.calcSpecialPrice("1983", priceCategory,
                null, receiptMock);

        assertFalse(entry.isPresent());
    }


    @Test
    void calcSpecialPrice2() {
        given(receiptMock.countEntries("1983")).willReturn(3L);

        BuyXGetYFree buyXGetYFree = new BuyXGetYFree();
        JSONObject priceCategory = ((JSONObject) testPricingSchema.get("1983"));

        Optional<Entry> entry = buyXGetYFree.calcSpecialPrice("1983", priceCategory,
                null, receiptMock);

        assertTrue(entry.isPresent());
        assertEquals(BigDecimal.valueOf(-1.99), entry.get().getPrice());
    }


    @Test
    void calcSpecialPrice3() {
        given(receiptMock.countEntries("1983")).willReturn(6L);

        BuyXGetYFree buyXGetYFree = new BuyXGetYFree();
        JSONObject priceCategory = ((JSONObject) testPricingSchema.get("1983"));

        Optional<Entry> entry = buyXGetYFree.calcSpecialPrice("1983", priceCategory,
                null, receiptMock);

        assertTrue(entry.isPresent());
        assertEquals(BigDecimal.valueOf(-1.99), entry.get().getPrice());
        assertEquals(2, entry.get().getQty());
    }

}