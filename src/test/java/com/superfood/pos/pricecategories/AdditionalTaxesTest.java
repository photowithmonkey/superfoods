package com.superfood.pos.pricecategories;

import com.superfood.pos.api.Receipt;
import com.superfood.pos.components.Entry;
import com.superfood.pos.demo.TestPricingSchemaBean;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@SpringBootTest
class AdditionalTaxesTest {

    @Autowired
    TestPricingSchemaBean testPricingSchemaBean;

    private JSONObject testPricingSchema;

    private JSONObject testPriceCategory;

    private JSONObject testAntiPriceCategory;

    @MockBean
    private Receipt testReceipt;


    @BeforeEach
    public void setup() {
        testPricingSchema = this.testPricingSchemaBean.getTestPricingSchema();
        testPriceCategory = ((JSONObject) testPricingSchema.get("0923"));
        testAntiPriceCategory = ((JSONObject) testPricingSchema.get("6732"));
    }

    @Test
    void customWillHandle() {
        AdditionalTaxes additionalTaxes = new AdditionalTaxes();
        boolean result = additionalTaxes.customWillHandle(testPriceCategory);
        assertTrue(result);

        boolean result2 = additionalTaxes.customWillHandle(testAntiPriceCategory);
        assertFalse(result2);
    }

    @Test
    void calcSpecialPrice() {

        given(testReceipt.countEntries("0923")).willReturn(1L);

        AdditionalTaxes additionalTaxes = new AdditionalTaxes();
        JSONObject priceCategory = ((JSONObject) testPricingSchema.get("0923"));
        Optional<Entry> entry = additionalTaxes.calcSpecialPrice("0923", priceCategory,
                null, testReceipt);

        assertTrue(entry.isPresent());
        assertEquals(new BigDecimal("1.432825"), entry.get().getPrice());
    }


    @Test
    void calcSpecialPrice2() {

        given(testReceipt.countEntries("0923")).willReturn(3L);

        AdditionalTaxes additionalTaxes = new AdditionalTaxes();
        JSONObject priceCategory = ((JSONObject) testPricingSchema.get("0923"));
        Optional<Entry> entry = additionalTaxes.calcSpecialPrice("0923", priceCategory,
                null, testReceipt);

        assertTrue(entry.isPresent());
        assertEquals(new BigDecimal("1.432825"), entry.get().getPrice());
        assertEquals(3, entry.get().getQty());
    }
}