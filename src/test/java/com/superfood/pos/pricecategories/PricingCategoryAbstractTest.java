package com.superfood.pos.pricecategories;

import com.superfood.pos.api.Handler;
import com.superfood.pos.api.Receipt;
import com.superfood.pos.components.Entry;
import com.superfood.pos.demo.TestPricingSchemaBean;
import com.superfood.pos.testdatabeans.TestPricingCategory;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.util.Optional;

import static com.superfood.pos.consts.CommonPriceCategoryConst.NAME;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@SpringBootTest
class PricingCategoryAbstractTest {

    @Autowired
    TestPricingSchemaBean testPricingSchemaBean;

    @MockBean
    Receipt testReceipt;

    private JSONObject testPricingSchema;

    @BeforeEach
    public void setup() {
        testPricingSchema = testPricingSchemaBean.getTestPricingSchema();
    }

    @MockBean
    TestPricingCategory testSuccessor;

    @Test
    void handle() {
        TestPricingCategory testPricingCategory = new TestPricingCategory();
        testPricingCategory.handle("6732", testPricingSchema, testReceipt);
        verify(testReceipt).addEntry(new Entry("6732", "Chips",new BigDecimal("2.49") , Entry.EntryType.Debit, 1L));
        verify(testReceipt).addEntry(new Entry("6732", "Chips",new BigDecimal("1.00") , Entry.EntryType.Credit, 1L));
    }

    @Test
    void willHandle() {
        TestPricingCategory testPricingCategory = new TestPricingCategory();
        JSONObject priceCategory = (JSONObject) testPricingSchema.get("6732");

        Handler result = testPricingCategory.willHandle(priceCategory);
        assertEquals(testPricingCategory, result);
    }

    @Test
    void willHandle2() {
        TestPricingCategory testPricingCategory = new TestPricingCategory(false);
        JSONObject priceCategory = (JSONObject) testPricingSchema.get("1983");
        Handler result = testPricingCategory.willHandle(priceCategory);
        assertNull(result);
    }

    @Test
    void willHandle3() {
        JSONObject priceCategory = (JSONObject) testPricingSchema.get("1983");
        TestPricingCategory expectedPriceCategoryObj = new TestPricingCategory();
        given(testSuccessor.willHandle(priceCategory)).willReturn(expectedPriceCategoryObj);

        TestPricingCategory testPricingCategory = new TestPricingCategory(false);
        testPricingCategory.setSuccessor(testSuccessor);
        Handler result = testPricingCategory.willHandle(priceCategory);
        assertEquals(expectedPriceCategoryObj, result);
    }
}

