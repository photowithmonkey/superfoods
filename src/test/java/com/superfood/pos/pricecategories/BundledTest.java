package com.superfood.pos.pricecategories;

import com.superfood.pos.api.LookupPriceCategory;
import com.superfood.pos.api.Receipt;
import com.superfood.pos.components.Entry;
import com.superfood.pos.demo.TestPricingSchemaBean;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@SpringBootTest
class BundledTest {

    @Autowired
    TestPricingSchemaBean testPricingSchemaBean;

    private JSONObject testPricingSchema;

    private JSONObject testPriceCategory;

    private JSONObject testAntiPriceCategory;

    @MockBean
    private Receipt receiptMock;

    @BeforeEach
    public void setup() {
        testPricingSchema = this.testPricingSchemaBean.getTestPricingSchema();
        testPriceCategory = ((JSONObject) testPricingSchema.get("6732"));
        testAntiPriceCategory = ((JSONObject) testPricingSchema.get("0923"));
    }

    @Test
    void customWillHandle() {
        Bundled bundled = new Bundled();
        boolean result = bundled.customWillHandle(testPriceCategory);
        assertTrue(result);

        boolean result2 = bundled.customWillHandle(testAntiPriceCategory);
        assertFalse(result2);
    }

    @Test
    void calcSpecialPrice() {
        Bundled bundled = new Bundled();
        JSONObject priceCategory = ((JSONObject) testPricingSchema.get("6732"));
        LookupPriceCategory boundPriceCategoryLookupFunc = bundled.getPriceCategoryFunc(testPricingSchema);

        Entry existingEntry = new Entry("6732", "Chips", new BigDecimal("2.49"), Entry.EntryType.Debit, 1L);
        Entry boundEntry = new Entry("4900", "Salsa", new BigDecimal("3.49"), Entry.EntryType.Debit, 1L);
        given(receiptMock.countEntries("6732")).willReturn(1L);
        given(receiptMock.countEntries("4900")).willReturn(1L);

        Optional<Entry> entry = bundled.calcSpecialPrice("6732", priceCategory,
                boundPriceCategoryLookupFunc, receiptMock);

        assertTrue(entry.isPresent());
        assertEquals(BigDecimal.valueOf(-0.99), entry.get().getPrice());
    }


    @Test
    void calcSpecialPrice2() {
        Bundled bundled = new Bundled();
        JSONObject priceCategory = ((JSONObject) testPricingSchema.get("6732"));
        LookupPriceCategory boundPriceCategoryLookupFunc = bundled.getPriceCategoryFunc(testPricingSchema);

        Entry existingEntry = new Entry("6732", "Chips", new BigDecimal("2.49"), Entry.EntryType.Debit, 1L);
        given(receiptMock.countEntries("6732")).willReturn(1L);
        given(receiptMock.countEntries("4900")).willReturn(0L);

        Optional<Entry> entry = bundled.calcSpecialPrice("6732", priceCategory,
                boundPriceCategoryLookupFunc, receiptMock);

        assertFalse(entry.isPresent());
    }

    @Test
    void calcSpecialPrice3() {
        Bundled bundled = new Bundled();
        JSONObject priceCategory = ((JSONObject) testPricingSchema.get("6732"));
        LookupPriceCategory boundPriceCategoryLookupFunc = bundled.getPriceCategoryFunc(testPricingSchema);

        given(receiptMock.countEntries("6732")).willReturn(1L);
        given(receiptMock.countEntries("4900")).willReturn(1L);

        Optional<Entry> entry = bundled.calcSpecialPrice("6732", priceCategory,
                boundPriceCategoryLookupFunc, receiptMock);

        assertTrue(entry.isPresent());
        assertEquals(BigDecimal.valueOf(-0.99), entry.get().getPrice());
    }


    @Test
    void calcSpecialPrice4() {
        Bundled bundled = new Bundled();
        JSONObject priceCategory = ((JSONObject) testPricingSchema.get("6732"));
        LookupPriceCategory boundPriceCategoryLookupFunc = bundled.getPriceCategoryFunc(testPricingSchema);

        given(receiptMock.countEntries("6732")).willReturn(1L);
        given(receiptMock.countEntries("4900")).willReturn(1L);

        Optional<Entry> entry = bundled.calcSpecialPrice("6732", priceCategory,
                boundPriceCategoryLookupFunc, receiptMock);

        assertTrue(entry.isPresent());
        assertEquals(BigDecimal.valueOf(-0.99), entry.get().getPrice());
        assertEquals(1, entry.get().getQty());
    }
}