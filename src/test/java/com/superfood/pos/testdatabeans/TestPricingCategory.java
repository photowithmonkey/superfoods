package com.superfood.pos.testdatabeans;

import com.superfood.pos.api.LookupPriceCategory;
import com.superfood.pos.api.Receipt;
import com.superfood.pos.components.Entry;
import com.superfood.pos.pricecategories.PricingCategoryAbstract;
import net.minidev.json.JSONObject;

import java.math.BigDecimal;
import java.util.Optional;

public class TestPricingCategory extends PricingCategoryAbstract {

    private boolean willHandle;

    public TestPricingCategory() {
        this(true);
    }

    public TestPricingCategory(boolean willHandle) {
        this.willHandle = willHandle;
    }

    @Override
    protected boolean customWillHandle(JSONObject offer) {
        return willHandle;
    }

    @Override
    protected Optional<Entry> calcSpecialPrice(String code, JSONObject priceScheme, LookupPriceCategory priceCategoryFunc, Receipt receipt) {
        Entry testEntry = new Entry("6732","Chips", new BigDecimal("1.00"), Entry.EntryType.Credit, 1L);
        return Optional.of(testEntry);
    }
}
