package com.superfood.pos.testdatabeans;

import com.superfood.pos.api.Receipt;
import com.superfood.pos.components.Entry;
import com.superfood.pos.components.ReceiptImpl;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class TestReceiptBean {

    public Receipt getTestReceipt() {
        Receipt receipt = new ReceiptImpl();
        receipt.addEntry(new Entry("4900", "Salsa", new BigDecimal("3.49"), Entry.EntryType.Debit, 1L));
        receipt.addEntry(new Entry("8873", "Milk", new BigDecimal("2.49"), Entry.EntryType.Debit, 1L));
        receipt.addEntry(new Entry("6732", "Chips", new BigDecimal("2.49"), Entry.EntryType.Debit, 1L));
        receipt.addEntry(new Entry("0923", "Wine", new BigDecimal("15.49"), Entry.EntryType.Debit, 1L));
        receipt.addEntry(new Entry("1983", "Toothbrush", new BigDecimal("1.99"), Entry.EntryType.Debit, 1L));
        receipt.addEntry(new Entry("1983", "Toothbrush", new BigDecimal("1.99"), Entry.EntryType.Debit, 1L));
        receipt.addEntry(new Entry("1983", "Toothbrush", new BigDecimal("1.99"), Entry.EntryType.Debit, 1L));
        receipt.addEntry(new Entry("1983", "Toothbrush", new BigDecimal("1.99"), Entry.EntryType.Debit, 1L));
        //sub-total: 31.92

        receipt.addEntry(new Entry("1983", "Toothbrush", new BigDecimal("1.99").negate(), Entry.EntryType.Credit, 1L));
        receipt.addEntry(new Entry("6732", "Chips", new BigDecimal("0.99").negate(), Entry.EntryType.Credit, 1L));
        receipt.addEntry(new Entry("0923", "Wine", new BigDecimal("1.43"), Entry.EntryType.Credit, 1L));
        //sub-total: -1.55

        //total: 30.37

        return receipt;
    }
}
