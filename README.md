# Supermarket Pricing Coding Exercise

### Summary

This code allows for flexible maintenance of pricing information as well as adding new sales offers without having
to alter the code other than introducing additional sales offer classes.

Since there is no static reference to the sales offers in the code new offers can be added just by:

1. Creating a new Class that extends the PricingCategoryAbstract
2. Annotating the class with Spring @Component attribute
3. Placing the class in the com.superfood.pros.pricategories package (not necessary, but tidy)

Spring will take care loading the class at runtime, and the 
use of the [Chain Of Responsibility Pattern](https://en.wikipedia.org/wiki/Chain-of-responsibility_pattern) allows the
code to act on corresponding price configurations with any explicit reference.

The specific pricing for the offers is passed in as a JSON text file, and then handled throughout the code using
 json-smart library.
 
This design would still require the application to be rebuild after new sales offer classes are added. A further elaboration
could include introducing a plugin architecture (like pf4j) which would storing each sales offer as a "plugin" in a directory,
and only restarting the system between changes. 

### Limitations

The system is designed to accomodate a single price offer per product. In real world one would probably want to allow 
for compositing price offers. For instance a bottle of wine could have a tax surcharge, but also be offered with a
buy 2 get 1 free offer. I tried adding that, but it would require changing the pricing schema, and I did not want to 
stray too far from the specifications. The system should, however, be able to deal with multiple discounts, i.e.
if someone buys four toothbrushes, they would get two free. However, there is no support on setting a limit on the
offer, as in "one offer per customer".

### Building and Running

The primary test is located in PosApplicationTests, this generates the prescribed transaction sequence and prints
register tape output. If you are running from the console, you can run all unit tests via:

```
mvn test
```

The application will also execute the main test sequence when run directly. If running from the console
it can be run via Maven:

```
mvn spring-boot:run
```

To build the jar via Maven run:

```
mvn package
```

You can then execute the created JAR (located in the target folder) via Java (8 or above) directly: 

```
java -jar pos-0.0.1-SNAPSHOT.jar
```


